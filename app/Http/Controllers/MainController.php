<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    public function home()
    {
        return view('home',[
            "title" => "Kevin",
            "nama" => "Kevin Rodrigo Semuel Ahab"
        ]);
    }

    public function about()
    {
        return view('about',[
            "title" => "About"
        ]);
    }

    public function contact()
    {
        return view('contact',[
            "title" => "Contact"
        ]);
    }

    public function postblog()
    {
        return view('postblog',[
            "title" => "Post Blog"
        ]);
    }

    public function blog()
    {
         return view('blog',[
        "title" => "Blog"
        ]);
    }
}
